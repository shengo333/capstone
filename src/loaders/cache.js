import { redisClient } from "../redis_client";




// Clear cache for user's CV data
const clearUserCvCache = (userId) => {
    const cacheKey = `cv:${userId}`;
    redisClient.del(cacheKey, (err, numDeleted) => {
      // ...
      console.log(cacheKey)
    });
  };
  

  
  export { clearUserCvCache };