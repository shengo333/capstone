import { Context, Loader } from '../interfaces/general';
import { User } from '../models/user.model';

import passport from 'passport';
import { Strategy, ExtractJwt } from 'passport-jwt';

export const jwtSecret = 'your-secret-key';

export const loadPassport: Loader = (app, context) => {
  const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtSecret,
  };

  passport.use(
    new Strategy(jwtOptions, async (payload, done) => {
      try {
        // Find the user by ID from the payload
        const user = await User.findOne({ where: { id: payload.id } });

        if (!user) {
          return done(null, false);
        }

        return done(null, user);
      } catch (error) {
        return done(error, false);
      }
    }),
  );

  return passport;
};
