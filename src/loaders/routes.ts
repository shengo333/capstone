import express from 'express';
import { Context } from '../interfaces/general';
import { makeAuthRouter } from '../routes/auth';
import { makeUserRouter } from '../routes/user';
import { makeExperience } from '../routes/experience';
import { makeFeedback } from '../routes/feedback';
import { makeProject } from '../routes/project';

export const loadRoutes = (app: express.Router, context: Context) => {
  app.use('/api/auth', makeAuthRouter(context));
  app.use('/api/users', makeUserRouter(context));
  app.use('/api/experience', makeExperience(context));
  app.use('/api/feedback', makeFeedback(context));
  app.use('/api/project', makeProject(context));
};
