import { DataTypes, Model, Optional, Sequelize } from 'sequelize';
import { Models } from '../interfaces/general';

interface FeedbackAttributes {
  id: number;
  from_user: number;
  to_user: number;
  content: string;
  company_name: string;
}

export class Feedback
  extends Model<FeedbackAttributes, Optional<FeedbackAttributes, 'id'>>
  implements FeedbackAttributes
{
  id: number;
  from_user: number;
  to_user: number;
  content: string;
  company_name: string;

  readonly createdAt: Date;
  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize) {
    Feedback.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        from_user: {
          field: 'from_user',
          type: DataTypes.INTEGER.UNSIGNED,
          allowNull: false,
          references: {
            model: 'User',
            key: 'id',
          },
        },
        to_user: {
          field: 'to_user',
          type: DataTypes.INTEGER.UNSIGNED,
          allowNull: false,
          references: {
            model: 'User',
            key: 'id',
          },
        },
        content: {
          type: DataTypes.STRING(256),
          allowNull: false,
        },
        company_name: {
          type: DataTypes.STRING(256),
          allowNull: false,
        },
      },
      {
        tableName: 'feedbacks',
        underscored: true,
        sequelize,
      },
    );
  }

  static associate(models: Models, sequelize: Sequelize) {
    // Feedback belongs to a User as "fromUser"
    Feedback.belongsTo(models.user, {
      foreignKey: 'from_user',
      as: 'fromUser',
    });

    // Feedback belongs to a User as "toUser"
    Feedback.belongsTo(models.user, { foreignKey: 'to_user', as: 'toUser' });
  }
}
