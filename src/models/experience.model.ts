import { DataTypes, Model, Optional, Sequelize } from 'sequelize';
import { Models } from '../interfaces/general';

interface ExperienceAttributes {
  id: number;
  user_id: number;
  company_name: string;
  role: string;
  description: string;
  startDate?: Date;
  endDate?: Date;
}

export class Experience
  extends Model<ExperienceAttributes, Optional<ExperienceAttributes, 'id'>>
  implements ExperienceAttributes
{
  id: number;
  user_id: number;
  company_name: string;
  role: string;
  description: string;

  readonly startDate: Date;
  readonly endDate: Date;

  readonly createdAt: Date;
  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize) {
    Experience.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        user_id: {
          field: 'user_id',
          type: DataTypes.INTEGER.UNSIGNED, // Corrected
          allowNull: false,
          references: {
            model: 'User',
            key: 'id',
          },
        },
        company_name: {
          field: 'company_name',
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        role: {
          field: 'role',
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        description: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
      },
      {
        tableName: 'experiences',
        underscored: true,
        sequelize,
      },
    );
  }

  static associate(models: Models, sequelize: Sequelize) {
    // Experience belongs to a User
    Experience.belongsTo(models.user, { foreignKey: 'user_id' });
  }
}
