import { Context, RouterFactory } from '../interfaces/general';
import express, { Response, Request } from 'express';
import passport from 'passport';
import bodyParser from 'body-parser';
import roles from '../middleware/checkroles';

const { body, query, validationResult } = require('express-validator');

// Import your User model here
import { User, UserRole } from '../models/user.model';
import { Feedback } from '../models/feedback.model';
import { clearUserCvCache } from '../loaders/cache';

const urlencodedParser = bodyParser.urlencoded({ extended: false });

export const makeFeedback: RouterFactory = (context: Context) => {
  const router = express.Router();

  router.post(
    '',
    urlencodedParser,
    passport.authenticate('jwt', { session: false }),
    roles(['Admin', 'User']),
    [
      // Validate the request body fields
      body('fromUser').isInt(),
      body('companyName').isString().isLength({ max: 255 }),
      body('toUser').isInt(),
      body('context').isString(),
    ],
    async (req: Request, res: Response) => {
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      const currentUser = req.user as User;


      const { fromUser, companyName, toUser, context } = req.body;

   // Check if the user is trying to create feedback for themselves
      if (fromUser === toUser) {
        return res.status(400).json({
          message: 'You cannot create feedback for yourself.',
        });
      }
      if (fromUser != currentUser.id) {
        return res.status(400).json({
          message: 'You cannot create feedback for someone else.',
        });
      }

      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      try {
        // Check if the 'toUser' exists and is a valid user in your system (you may need to add this validation)

        // Create a new feedback entry
        const newFeedback = await Feedback.create({
          from_user: req.body.fromUser,
          company_name: req.body.companyName,
          to_user: req.body.toUser,
          content: req.body.context,
        });
        await clearUserCvCache(currentUser.id);

        // Return the newly created feedback entry in the response
        return res.status(201).json({
          id: newFeedback.id,
          fromUser: newFeedback.from_user,
          companyName: newFeedback.company_name,
          toUser: newFeedback.to_user,
          context: newFeedback.content,
        });
      } catch (error) {
        console.error(error);
        return res.status(500).json({
          error: 'Something went wrong on the server.',
        });
      }
    },
  );

  router.get(
    '/',
    urlencodedParser,
    passport.authenticate('jwt', { session: false }),
    roles(['Admin']),
    query('pageSize').notEmpty(),
    query('page').notEmpty(),
    async (req: Request, res: Response) => {
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      const currentUser = req.user as User;

      // Extract query parameters for pagination
      const pageSize = Number(req.query.pageSize) || 10;
      const page = Number(req.query.page) || 1;

      try {
        // Find feedback entries with pagination and count total records
        const { rows: feedbackEntries, count: totalCount } =
          await Feedback.findAndCountAll({
            limit: pageSize,
            offset: (page - 1) * pageSize,
          });

        // Map feedback entries to the desired response format
        const formattedFeedbackEntries = feedbackEntries.map((feedback) => ({
          id: feedback.id,
          fromUser: feedback.from_user,
          companyName: feedback.company_name,
          toUser: feedback.to_user,
          context: feedback.content,
        }));

        // Return feedback entries, total count, and X-total-count header in the response
        res.setHeader('X-total-count', totalCount);
          await clearUserCvCache(currentUser.id);

        return res.status(200).json(formattedFeedbackEntries);
      } catch (error) {
        console.error(error);
        return res.status(500).json({
          error: 'Something went wrong on the server.',
        });
      }
    },
  );

  router.get('/:id', urlencodedParser, async (req, res) => {
    // Log something using the logger
    req.log.info('This is a log message from the route handler');
    try {
      const feedbackId = parseInt(req.params.id, 10);

      if (isNaN(feedbackId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      // Find the feedback entry by ID
      const feedback = await Feedback.findByPk(feedbackId);

      if (!feedback) {
        // If no feedback entry is found, return a 404 Not Found response
        return res.status(404).json({
          message: 'Feedback with the provided ID does not exist.',
        });
      }

      // Return the feedback entry in the response
      return res.status(200).json({
        id: feedback.id,
        fromUser: feedback.from_user,
        companyName: feedback.company_name,
        toUser: feedback.to_user,
        context: feedback.content,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        error: 'Something went wrong on the server.',
      });
    }
  });

  router.put(
    '/:id',
    urlencodedParser,
    passport.authenticate('jwt', { session: false }),
    [
      body('fromUser').isInt(),
      body('toUser').isInt(),
      body('context').isString().isLength({ max: 256 }),
      body('companyName').isString().isLength({ max: 256 }),
    ],
    async (req: Request, res: Response) => {
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      const currentUser = req.user as User;
      const feedbackId = parseInt(req.params.id, 10); // Parse the ID as an integer

      // Validate that feedbackId is a number
      if (isNaN(feedbackId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      try {

        // Check if there are validation errors in the request body
        const validationErrors = validationResult(req);
        if (!validationErrors.isEmpty()) {
          return res.status(400).json({ errors: validationErrors.array() });
        }

        // Find the feedback entry by ID
        const feedback = await Feedback.findByPk(feedbackId);

        if (!feedback) {
          // If no feedback entry is found, return a 404 Not Found response
          return res.status(404).json({
            message: 'Feedback not found.',
          });
        }

        // Check if the user is an Admin or the owner of the feedback entry
        if (
          currentUser.role === UserRole.Admin ||
          feedback.from_user === currentUser.id
        ) {
          // If Admin or owner, proceed with the update
          // Validate and extract request body
          const { fromUser, companyName, toUser, context } = req.body;

          // Update the feedback entry
          await feedback.update({
            from_user: fromUser,
            company_name: companyName,
            to_user: toUser,
            content: context,
          });

          // Retrieve the updated feedback
          const updatedFeedback = await Feedback.findByPk(feedbackId);

          await clearUserCvCache(currentUser.id);
          (console.log('it worked'))


          // Return the updated feedback in the response
          return res.status(200).json({
            id: updatedFeedback.id,
            fromUser: updatedFeedback.from_user,
            companyName: updatedFeedback.company_name,
            toUser: updatedFeedback.to_user,
            context: updatedFeedback.content,
          });
        } else {
          // If the user is neither an Admin nor the owner, return a 403 Forbidden response
          return res.status(403).json({
            message:
              'Permission denied. You are not authorized to update this feedback.',
          });
        }
      } catch (error) {
        console.error(error);
        return res.status(500).json({
          error: 'Something went wrong on the server.',
        });
      }
    },
  );

  router.delete(
    '/:id',
    urlencodedParser,
    passport.authenticate('jwt', { session: false }),
    async (req, res) => {
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      const currentUser = req.user as User;
      const feedbackId = parseInt(req.params.id, 10);

      // Validate that feedbackId is a number
      if (isNaN(feedbackId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      try {
        // Find the feedback entry by ID
        const feedback = await Feedback.findByPk(feedbackId);

        if (!feedback) {
          // If no feedback entry is found, return a 404 Not Found response
          return res.status(404).json({
            message: 'Feedback not found.',
          });
        }

        // Check if the user is an Admin or the owner of the feedback entry
        if (
          currentUser.role === UserRole.Admin ||
          feedback.from_user === currentUser.id
        ) {
          // If Admin or owner, proceed with the deletion
          await feedback.destroy();
          await clearUserCvCache(currentUser.id);

          // Return a 204 No Content response to indicate successful deletion
          return res.status(204).send();
        } else {
          // If the user is neither an Admin nor the owner, return a 403 Forbidden response
          return res.status(403).json({
            message:
              'Permission denied. You are not authorized to delete this feedback.',
          });
        }
      } catch (error) {
        console.error(error);
        return res.status(500).json({
          error: 'Something went wrong on the server.',
        });
      }
    },
  );

  return router;
};
