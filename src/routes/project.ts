import { Context, RouterFactory } from '../interfaces/general';
import express, { Response, Request } from 'express';
import passport from 'passport';
import bcrypt from 'bcrypt';
import bodyParser from 'body-parser';
import roles from '../middleware/checkroles';
import { clearUserCvCache } from '../loaders/cache';
const { body, query, validationResult } = require('express-validator');

import multer from 'multer';

// Import your User model here
import { User, UserRole } from '../models/user.model';
import { Project } from '../models/project.model';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'projectimages/'); // Specify the directory where files will be stored
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
    const ext = file.originalname.split('.').pop(); // Get the file extension
    const filename = `${file.fieldname}-${uniqueSuffix}.${ext}`;
    cb(null, filename);
  },
});

const upload = multer({ storage: storage });
const urlencodedParser = bodyParser.urlencoded({ extended: false });

export const makeProject: RouterFactory = (context: Context) => {
  const router = express.Router();

  // Add the necessary imports and configurations for multer (file upload) here

  // Update the route handler for POST /api/project
  router.post(
    '',
    urlencodedParser,
    upload.single('image'), // Use multer middleware for file upload
    passport.authenticate('jwt', { session: false }),
    roles(['Admin', 'User']),
    [
      body('user_id').isInt(),
      body('image').isString().isLength({ max: 256 }),
      body('description').isString().isLength({ max: 256 }),
    ],
    async (req: Request, res: Response) => {
      // Log something using the logger
      req.log.info('This is a log message from the route handler');

      const currentUser = req.user as User;

      const { userId, description } = req.body;

      try {
        const image = req.file ? req.file.filename : null;

        // Create a new project in the database
        const project = await Project.create({
          user_id: userId,
          image: image,
          description: description,
        });
        await clearUserCvCache(currentUser.id);

        res.status(201).json({
          id: project.id,
          userId: project.user_id,
          image: project.image,
          description: project.description,
        });
      } catch (error) {
        console.error(error);
        return res
          .status(500)
          .json({ error: 'Something went wrong on the server.' });
      }
    },
  );

  router.get(
    '',
    passport.authenticate('jwt', { session: false }),
    roles(['Admin']),
    query('pageSize').notEmpty(),
    query('page').notEmpty(),
    async (req: Request, res: Response) => {
      const currentUser = req.user as User;
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      // if (currentUser.role !== UserRole.Admin) {
      //   return res.status(403).json({
      //     message: 'Permission denied. You are not authorized to access this resource.',
      //   });
      // }

      try {
        const pageSize = Number(req.query.pageSize); // Parse pageSize as a number
        const page = Number(req.query.page); // Parse page as a number

        if (isNaN(pageSize) || isNaN(page)) {
          return res.status(400).json({
            message:
              'Invalid pageSize or page value. Please provide valid numbers.',
          });
        }

        // Calculate the offset based on pageSize and page number
        const offset = (page - 1) * pageSize;

        // Retrieve projects with pagination and count total records
        const { rows: projects, count: totalCount } =
          await Project.findAndCountAll({
            limit: pageSize,
            offset: offset,
          });

        // Return projects and total count in the response
        res.status(200).json({
          projects,
          totalCount,
        });
      } catch (error) {
        console.error(error);
        return res
          .status(500)
          .json({ error: 'Something went wrong on the server.' });
      }
    },
  );

  router.get('/:id', async (req: Request, res: Response) => {
    // Log something using the logger
    req.log.info('This is a log message from the route handler');
    try {
      const projectId = parseInt(req.params.id, 10);
      if (isNaN(projectId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      // Fetch the project by ID from the database
      const project = await Project.findByPk(projectId);

      if (!project) {
        return res.status(404).json({ message: 'Project not found' });
      }

      // Return project data
      res.status(200).json({
        id: project.id,
        userId: project.user_id,
        image: project.image,
        description: project.description,
      });
    } catch (error) {
      console.error(error);
      return res
        .status(500)
        .json({ error: 'Something went wrong on the server.' });
    }
  });

  // Update a project's details
  router.put(
    '/:id',
    urlencodedParser,
    passport.authenticate('jwt', { session: false }),
    [
      body('user_id').isInt(),
      body('image').isString().isLength({ max: 256 }),
      body('description').isString().isLength({ max: 256 }),
    ],
    async (req: Request, res: Response) => {
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      const currentUser = req.user as User;
      const projectId = req.params.id;

      try {
        // Fetch the project by ID from the database
        const project = await Project.findByPk(projectId);

        if (!project) {
          return res.status(404).json({ message: 'Project not found' });
        }
        // console.log(req.body, 'kjasndkjnadkjnajkhsbdjhabsdjhbasjhdbasjhbdjhasbdjhbasdjhbasjhdbjhasbdjhasbdjhabsdjhbasjhbdjhasbdjhabsdjhbasjhbdjhasbdhjbasjhdbasjhbdjhasbdjhbsadjhbasjhdbajshbdjhasbdjhbasdjhbasjhdb')
        // Check if the authenticated user is either the owner of the project or an admin
        if (
          currentUser.role !== UserRole.Admin &&
          project.user_id !== currentUser.id
        ) {
          return res.status(403).json({
            message: 'You do not have permission to update this project.',
          });
        }

        // Parse and validate the request body
        const { userId, image, description } = req.body;

        // Update project fields
        project.user_id = userId;
        project.image = image;
        project.description = description;

        // Save the updated project details to the database
        await project.save();

        // Return the updated project data
        await clearUserCvCache(currentUser.id);

        res.status(200).json({
          id: project.id,
          userId: project.user_id,
          image: project.image,
          description: project.description,
        });
      } catch (error) {
        console.error(error);
        return res
          .status(500)
          .json({ error: 'Something went wrong on the server.' });
      }
    },
  );

  // DELETE a project
  router.delete(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    async (req: Request, res: Response) => {
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      const currentUser = req.user as User;
      const projectId = parseInt(req.params.id, 10);
      if (isNaN(projectId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      try {
        // Fetch the project by ID from the database
        const project = await Project.findByPk(projectId);

        if (!project) {
          return res.status(404).json({ message: 'Project not found' });
        }

        // Check if the authenticated user is either the owner of the project or an admin
        if (
          currentUser.role !== UserRole.Admin &&
          project.user_id !== currentUser.id
        ) {
          return res.status(403).json({
            message: 'You do not have permission to delete this project.',
          });
        }

        // Delete the project
        await project.destroy();
        await clearUserCvCache(currentUser.id);

        // Return a 204 response indicating successful deletion
        return res.sendStatus(204);
      } catch (error) {
        console.error(error);
        return res
          .status(500)
          .json({ error: 'Something went wrong on the server.' });
      }
    },
  );

  return router;
};
