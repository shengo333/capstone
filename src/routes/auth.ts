import { Context, RouterFactory } from '../interfaces/general';
import express, { Response, Request } from 'express';
import bodyParser from 'body-parser';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import multer from 'multer';
import { body, validationResult } from 'express-validator';


// Import your User model here
import { User, UserRole } from '../models/user.model';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/'); // Specify the directory where files will be stored
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
    const ext = file.originalname.split('.').pop(); // Get the file extension
    const filename = `${file.fieldname}-${uniqueSuffix}.${ext}`;
    cb(null, filename);
  },
});

const upload = multer({ storage: storage });

const jsonParser = bodyParser.json();
const urlencodedParser = bodyParser.urlencoded({ extended: false });

export const makeAuthRouter: RouterFactory = (context: Context) => {
  const router = express.Router();

  // Define registration route
  router.post(
    '/register',
    urlencodedParser,
    upload.single('image'),
    [
      // Validate the request body fields for the registration route
      body('firstName').isString().notEmpty(),
      body('lastName').isString().notEmpty(),
      body('title').isString().optional(),
      body('summary').isString().optional(),
      body('email').isEmail(),
      body('password').isLength({ min: 6 }),
    ],
    async (req: Request, res: Response) => {
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
      const { firstName, lastName, title, summary, email, password } = req.body;

      try {
        const image = req.file ? req.file.filename : null;
        // Hash the password
        const hashedPassword = await bcrypt.hash(password, 10);
        const role = UserRole.User;

        // Create a new user in the database
        const user = await User.create({
          firstName,
          lastName,
          title,
          summary,
          email,
          password: hashedPassword,
          image, // Provide the image value
          role, // Provide the role value
        });

        // Generate a JWT token for the user
        const token = jwt.sign({ id: user.id }, 'your-secret-key', {
          expiresIn: '1h',
        });

        res.status(201).json({
          user: {
            id: user.id,
            firstName: user.firstName,
            lastName: user.lastName,
            title: user.title,
            summary: user.summary,
            email: user.email,
            image: user.image, // Include image in the response
            role: user.role, // Include role in the response
          },
          token,
        });
      } catch (error) {
        console.error(error);
        res.status(400).json({ error: 'Registration failed' });
      }
    },
  );

  // Define login route
  router.post(
    '/login',
    urlencodedParser,
    [
      // Validate the request body fields for the login route
      body('email').isEmail().withMessage('Invalid email format'),
      body('password').isLength({ min: 6 }).withMessage('Password must be at least 6 characters'),
    ],
    async (req: Request, res: Response) => {

      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
      const { email, password } = req.body;
      // Find the user by email in the database
      const user = await User.findOne({ where: { email } });

      if (!user) {
        return res.status(400).json({ error: 'Invalid credentials' });
      }

      // Check if the provided password matches the stored hash
      const passwordMatch = await bcrypt.compare(password, user.password);

      if (!passwordMatch) {
        return res.status(400).json({ error: 'Invalid credentials' });
      }

      // Generate a JWT token for the user
      const token = jwt.sign({ id: user.id }, 'your-secret-key', {
        expiresIn: '1h',
      });

      // Return the user and token in the response
      res.status(200).json({
        user: {
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.title,
          summary: user.summary,
          email: user.email,
          image: user.image, // Include image in the response
          role: user.role, // Include role in the response
        },
        token,
      });
    },
  );

  return router;
};
