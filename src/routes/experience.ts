import { Context, RouterFactory } from '../interfaces/general';
import express, { Response, Request } from 'express';
import passport from 'passport';
import bodyParser from 'body-parser';
import roles from '../middleware/checkroles';
import { clearUserCvCache } from '../loaders/cache';

const { body,query, validationResult } = require('express-validator');


// Import your User model here
import { User, UserRole } from '../models/user.model';
import { Experience } from '../models/experience.model';

const urlencodedParser = bodyParser.urlencoded({ extended: false });

export const makeExperience: RouterFactory = (context: Context) => {
  const router = express.Router();

  router.post(
    '',
    urlencodedParser,
    passport.authenticate('jwt', { session: false }),
    roles(['Admin', 'User']),
    [
      // Validate the request body fields
      body('user_id').isInt(),
      body('company_name').isString().isLength({ max: 255 }),
      body('role').isString().isLength({ max: 255 }),
      body('startDate').isDate(),
      body('endDate').isDate(),
      body('description').isString(),
      // Validate the 'role' field, ensuring it's one of the expected roles
      body('role').isIn(['Admin', 'User']),
    ],
    async (req: Request, res: Response) => {
      const currentUser = req.user as User;

      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
      // Log something using the logger
      req.log.info('This is a log message from the route handler');


      const { user_id, company_name, role, startDate, endDate, description } =
        req.body;

      try {
        // Create a new experience entry
        const newExperience = await Experience.create({
          user_id,
          company_name,
          role,
          startDate, // Assuming you have startDate and endDate in your request body
          endDate,
          description,
        });
        await clearUserCvCache(currentUser.id);

        // Return the newly created experience entry in the response
        return res.status(201).json(newExperience);
      } catch (error) {
        console.error(error);
        return res.status(500).json({
          error: 'Something went wrong on the server.',
        });
      }
    },
  );

  router.get(
    '/',
    urlencodedParser,
    passport.authenticate('jwt', { session: false }),
    roles(['Admin']),
    query('pageSize').notEmpty(),
    query('page').notEmpty(),
    async (req: Request, res: Response) => {
      const currentUser = req.user as User;

      // Log something using the logger
      req.log.info('This is a log message from the route handler');


      // Extract query parameters for pagination
      const pageSize = Number(req.query.pageSize) || 10;
      const page = Number(req.query.page) || 1;

      try {
        // Find experiences with pagination and count total records
        const { rows: experiences, count: totalCount } =
          await Experience.findAndCountAll({
            limit: pageSize,
            offset: (page - 1) * pageSize,
          });

        // Return experiences and total count in the response
        return res.status(200).json({
          experiences,
          totalCount,
        });
      } catch (error) {
        console.error(error);
        return res.status(500).json({
          error: 'Something went wrong on the server.',
        });
      }
    },
  );
  router.get('/:id', urlencodedParser, async (req: Request, res: Response) => {
    // Log something using the logger
    req.log.info('This is a log message from the route handler');
    try {
      const experienceId = parseInt(req.params.id, 10); // Parse the ID as an integer

      if (isNaN(experienceId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      // Find the experience by ID
      const experience = await Experience.findByPk(experienceId);

      if (!experience) {
        // If no experience is found, return a 404 Not Found response
        return res.status(404).json({
          message: 'Experience with the provided ID does not exist.',
        });
      }

      // If experience is found, return it in the response
      return res.status(200).json({
        id: experience.id,
        userId: experience.user_id,
        companyName: experience.company_name,
        role: experience.role,
        startDate: experience.startDate,
        endDate: experience.endDate,
        description: experience.description,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        error: 'Something went wrong on the server.',
      });
    }
  });

  router.put(
    '/:id',
    urlencodedParser,
    passport.authenticate('jwt', { session: false }),
    [
      body('user_id').isInt().withMessage('User ID must be an integer.'),
      body('company_name')
        .isString()
        .isLength({ max: 256 })
        .withMessage('Company name must be a string, up to 256 characters.'),
      body('role')
        .isString()
        .isLength({ max: 256 })
        .withMessage('Role must be a string, up to 256 characters.'),
      body('startDate').isISO8601().withMessage('Start date must be a valid ISO8601 date.'),
      body('endDate').optional({ nullable: true }).isISO8601().withMessage('End date must be a valid ISO8601 date, or null.'),
      body('description')
        .isString()
        .isLength({ max: 256 })
        .withMessage('Description must be a string, up to 256 characters.'),
    ],
    async (req: Request, res: Response) => {
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      const currentUser = req.user as User;
      const experienceId = parseInt(req.params.id, 10); // Parse the ID as an integer

      // Validate that experienceId is a number
      if (isNaN(experienceId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      try {
        // Check if the user is an admin
        if (currentUser.role === UserRole.Admin) {
          // If admin, proceed with the update
          // Validate and extract request body
          const { userId, companyName, role, startDate, endDate, description } =
            req.body;

          // Update the experience entry
          const [updatedCount] = await Experience.update(
            {
              user_id: userId,
              company_name: companyName,
              role,
              startDate,
              endDate,
              description,
            },
            {
              where: {
                id: experienceId,
              },
            },
          );

          if (updatedCount === 0) {
            return res.status(404).json({
              message: ' nothing to update',
            });
          }

          // Retrieve the updated experience
          const updatedExperience = await Experience.findByPk(experienceId);

          // Return the updated experience in the response
          return res.status(200).json({
            id: updatedExperience.id,
            userId: updatedExperience.user_id,
            companyName: updatedExperience.company_name,
            role: updatedExperience.role,
            startDate: updatedExperience.startDate,
            endDate: updatedExperience.endDate,
            description: updatedExperience.description,
          });
        } else {
          // If not an admin, check if the user is the owner of the experience
          const experience = await Experience.findOne({
            where: {
              id: experienceId,
              user_id: currentUser.id,
            },
          });

          if (!experience) {
            return res.status(404).json({
              message: 'Experience not found.',
            });
          }

          // Validate and extract request body
          const { userId, companyName, role, startDate, endDate, description } =
            req.body;

          // Update the experience entry
          const [updatedCount] = await Experience.update(
            {
              user_id: userId,
              company_name: companyName,
              role,
              startDate,
              endDate,
              description,
            },
            {
              where: {
                id: experienceId,
              },
            },
          );

          if (updatedCount === 0) {
            return res.status(404).json({
              message: 'Experience not found.',
            });
          }

          // Retrieve the updated experience
          const updatedExperience = await Experience.findByPk(experienceId);
          await clearUserCvCache(currentUser.id);

          // Return the updated experience in the response
          return res.status(200).json({
            id: updatedExperience.id,
            userId: updatedExperience.user_id,
            companyName: updatedExperience.company_name,
            role: updatedExperience.role,
            startDate: updatedExperience.startDate,
            endDate: updatedExperience.endDate,
            description: updatedExperience.description,
          });
        }
      } catch (error) {
        console.error(error);
        return res.status(500).json({
          error: 'Something went wrong on the server.',
        });
      }
    },
  );
  router.delete(
    '/:id',
    urlencodedParser,
    passport.authenticate('jwt', { session: false }),
    async (req: Request, res: Response) => {
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      const currentUser = req.user as User;
      const experienceId = parseInt(req.params.id, 10); // Parse the ID as an integer

      if (isNaN(experienceId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      try {
        // Check if the user has the necessary role to access this endpoint
        if (currentUser.role !== UserRole.Admin) {
          // If not an admin, check if the user is the owner of the experience
          const experience = await Experience.findOne({
            where: {
              id: experienceId,
              user_id: currentUser.id,
            },
          });

          if (!experience) {
            return res.status(404).json({
              message: 'Experience not found.',
            });
          }
        }

        // Delete the experience entry
        const deletedRows = await Experience.destroy({
          where: {
            id: experienceId,
          },
        });

        if (deletedRows === 0) {
          return res.status(404).json({
            message: 'Experience not found.',
          });
        }
        const deletedExperience = await Experience.findByPk(experienceId);
        await clearUserCvCache(currentUser.id);

        // Return a 204 No Content response if the experience is deleted successfully
        return res.status(204).send('user has been deleted');
      } catch (error) {
        console.error(error);
        return res.status(500).json({
          error: 'Something went wrong on the server.',
        });
      }
    },
  );

  return router;
};
