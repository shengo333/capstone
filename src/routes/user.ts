import { Context, RouterFactory } from '../interfaces/general';
import express, { Response, Request } from 'express';
import passport from 'passport';
import bcrypt from 'bcrypt';
import bodyParser from 'body-parser';
import roles from '../middleware/checkroles';
import { clearUserCvCache } from '../loaders/cache';

const { body, validationResult, query } = require('express-validator');


import multer from 'multer';

// Import your User model here
import { User, UserRole } from '../models/user.model';
import { Experience } from '../models/experience.model';
import { Project } from '../models/project.model';
import { Feedback } from '../models/feedback.model';
import { redisClient } from '../redis_client';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/'); // Specify the directory where files will be stored
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
    const ext = file.originalname.split('.').pop(); // Get the file extension
    const filename = `${file.fieldname}-${uniqueSuffix}.${ext}`;
    cb(null, filename);
  },
});

const upload = multer({ storage: storage });
const urlencodedParser = bodyParser.urlencoded({ extended: false });

export const makeUserRouter: RouterFactory = (context: Context) => {
  const router = express.Router();


  // Middleware to handle caching
const cacheMiddleware = (req: Request, res: Response, next: any) => {
  const userId = req.params.userId;
  const cacheKey = `cv:${userId}`;

  redisClient.get(cacheKey, (err, data) => {
    if (err) {
      console.error('Error getting data from cache:', err);
      next();
    }

    if (data) {
      // res.status(200).json(JSON.parse(data));
      res.status(200).json(JSON.parse(data));
    } else {
      next();
    }
  });
};

  router.post(
    '',
    urlencodedParser,
    upload.single('image'),
    passport.authenticate('jwt', { session: false }),
    [
      // Add validation rules for the request body fields
      body('firstName').isString().isLength({ max: 255 }),
      body('lastName').isString().isLength({ max: 255 }),
      body('title').isString().isLength({ max: 255 }),
      body('summary').isString().isLength({ max: 255 }),
      body('email').isEmail().isLength({ max: 255 }),
      body('password').isString().isLength({ min: 8, max: 255 }),
      // Validate the 'role' field, ensuring it's not included or set correctly
      body('role').not().exists(),
    ],
    async (req: Request, res: Response) => {
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      const currentUser = req.user as User;

      if (currentUser.role != UserRole.Admin) {
        return res.status(400).json({
          message: 'validation failed you are not admin',
        });
      }
      

      const { firstName, lastName, title, summary, email, password, role } =
        req.body;


      if(!password) {
        return res.status(400).send("password required")
      }
      try {
        // console.log('we still try');
        const image = req.file ? req.file.filename : null;
        const hashedPassword = await bcrypt.hash(password, 10);

        // Create a new user in the database
        const user = await User.create({
          firstName,
          lastName,
          title,
          summary,
          email,
          image,
          password: hashedPassword,
          role,
        });
        await clearUserCvCache(currentUser.id);

        res.status(201).json({
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.title,
          summary: user.summary,
          image: user.image,
          email: user.email,
          role: user.role,
        });
      } catch (error) {
        console.error(error);
        return res
          .status(505)
          .json({ error: 'Something went wrong on the server.' });
      }
    },
  );

  router.get(
    '',
    passport.authenticate('jwt', { session: false }),
    roles(['Admin']),
    query('pageSize').notEmpty(),
    query('page').notEmpty(),
    async (req: Request, res: Response) => {
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      const currentUser = req.user as User;

      // if (currentUser.role !== UserRole.Admin) {
      //   return res.status(400).json({
      //     message: 'Validation failed: You are not an admin.',
      //   });
      // }
      try {
        const pageSize = Number(req.query.pageSize); // Parse pageSize as a number
        const page = Number(req.query.page); // Parse page as a number

        if (isNaN(pageSize) || isNaN(page)) {
          return res.status(400).json({
            message:
              'Invalid pageSize or page value. Please provide valid numbers.',
          });
        }

        const users = await User.findUsersWithPagination(pageSize, page);
        res.status(201).json({
          users,
        });
        // ... (return the users with totalCount as shown in the previous response)
      } catch (error) {
        console.error(error);
        return res
          .status(500)
          .json({ error: 'Something went wrong on the server.' });
      }
    },
  );
  router.get('/:id', async (req: Request, res: Response) => {
    // Log something using the logger
    req.log.info('This is a log message from the route handler');
    try {
      const userId = parseInt(req.params.id, 10);

      if (isNaN(userId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }
      // Fetch the user by ID from the database (similar to your previous route)
      const user = await User.findByPk(userId);

      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }

      // Return user data
      res.status(200).json({
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        title: user.title,
        summary: user.summary,
        email: user.email,
        password: user.password, // Note: You might want to exclude sensitive data like passwords
        role: user.role,
      });
    } catch (error) {
      console.error(error);
      return res
        .status(500)
        .json({ error: 'Something went wrong on the server.' });
    }
  });

  // Update a user's profile
  router.put(
    '/:id',
    urlencodedParser,

    passport.authenticate('jwt', { session: false }),
    upload.single('image'),
    [      body('firstName').isString().isLength({ max: 255 }),
    body('lastName').isString().isLength({ max: 255 }),
    body('title').isString().isLength({ max: 255 }),
    body('summary').isString().isLength({ max: 255 }),
    body('email').isEmail().isLength({ max: 255 }),
    // Validate the 'role' field, ensuring it's not included or set correctly
    body('role').not().exists(),],


    async (req: Request, res: Response) => {
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      const currentUser = req.user as User;
      const userId = req.params.id;

      try {
        // Fetch the user by ID from the database
        const user = await User.findByPk(userId);

        if (!user) {
          return res.status(404).json({ message: 'User not found' });
        }

        // Check if the authenticated user is either the owner of the account or an admin
        if (
          currentUser.id !== parseInt(userId, 10)
          // &&
          // currentUser.role !== UserRole.Admin
        ) {
          return res.status(403).json({
            message: 'You do not have permission to update this account.',
          });
        }

        // Parse and validate the request body
        const { firstName, lastName, title, summary, email, password, role } =
          req.body;

        // Update user profile fields
        user.firstName = firstName;
        user.lastName = lastName;
        user.title = title;
        user.summary = summary;
        user.email = email;
        user.role = role;

        // Hash the password if provided
        if (password) {
          const hashedPassword = await bcrypt.hash(password, 10);
          user.password = hashedPassword;
        }

        // Generate a unique filename for a new image if uploaded
        if (req.file) {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          const ext = req.file.originalname.split('.').pop();
          const filename = `${req.file.fieldname}-${uniqueSuffix}.${ext}`;
          user.image = filename;
        }

        // Save the updated user profile to the database
        await user.save();
        await clearUserCvCache(currentUser.id);

        // Return the updated user data
        res.status(200).json({
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.title,
          summary: user.summary,
          email: user.email,
          role: user.role,
        });
      } catch (error) {
        console.error(error);
        return res
          .status(500)
          .json({ error: 'Something went wrong on the server.' });
      }
    },
  );
  // DELETE a user's account
  router.delete(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    async (req: Request, res: Response) => {
      console.log(req.params.id, 'jnasdkjnaj id is hereeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee')
      // Log something using the logger
      req.log.info('This is a log message from the route handler');
      const currentUser = req.user as User;
      const userId = req.params.id;

      const userIdN = parseInt(req.params.id, 10);

      if (isNaN(userIdN)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      try {
        // Fetch the user by ID from the database
        const user = await User.findByPk(userId);

        if (!user) {
          return res.status(404).json({ message: 'User not found' });
        }

        // Check if the authenticated user is either the owner of the account or an admin
        if (
          currentUser.id !== parseInt(userId, 10) &&
          currentUser.role !== UserRole.Admin
        ) {
          return res.status(403).json({
            message: 'You do not have permission to delete this account.',
          });
        }

        // Delete the user account
        await user.destroy();
        await clearUserCvCache(currentUser.id);

        // Return a 204 response indicating successful deletion
        return res.sendStatus(204).json({
          message: 'User user account.',
        });
      } catch (error) {
        console.error(error);
        // return 
        // res
        //   .status(500)
          // .json({ error: 'Something went wrong on the server.' });
      }
    },
  );
// Your route handler with caching
router.get('/:userId/cv', cacheMiddleware, async (req: Request, res: Response) => {
  req.log.info('This is a log message from the route handler');

  try {
    const userId = req.params.userId;
    const cacheKey = `cv:${userId}`;

    // Fetch the user by ID from the database
    const user = await User.findByPk(userId);

    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Fetch experiences, projects, and feedbacks related to the user
    const experiences = await Experience.findAll({
      where: { user_id: userId },
    });
    const projects = await Project.findAll({ where: { user_id: userId } });
    const feedbacks = await Feedback.findAll({
      where: { from_user: userId },
      include: [
        {
          model: User,
          as: 'toUser',
          attributes: ['id', 'firstName', 'lastName'],
        },
      ],
    });

    // Create a custom CV response object
    const cv = {
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      title: user.title,
      image: user.image,
      summary: user.summary,
      email: user.email,
      experiences: experiences.map((experience) => ({
        userId: experience.user_id,
        companyName: experience.company_name,
        role: experience.role,
        startDate: experience.startDate,
        endDate: experience.endDate,
        description: experience.description,
      })),
      projects: projects.map((project) => ({
        id: project.id,
        userId: project.user_id,
        image: project.image,
        description: project.description,
      })),
      feedbacks: feedbacks.map((feedback) => ({
        id: feedback.id,
        fromUser: feedback.from_user,
        companyName: feedback.company_name,
        to_user: feedback.to_user,
        context: feedback.content,
      })),
    };

    // Cache the data with a TTL of 1 hour (adjust as needed)
    redisClient.setex(cacheKey, 3600, JSON.stringify(cv));

    // Return the custom CV response
    res.status(200).json(cv);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: 'Something went wrong on the server.' });
  }
});


  return router;
};
