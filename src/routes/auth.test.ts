// Import necessary modules and functions
import path from "path";
import request  from 'supertest';

import {loadApp} from '../loaders/app'; // Adjust the path to your app loader
import { redisClient } from '../redis_client';

beforeAll(async () => {
  // Set up any necessary configurations.
  // For example, you might want to connect to a test database.
  // Ensure that your test environment connects to the test database.
});

afterAll(async () => {
  // Clean up resources, close the database connection, etc.
  // For example, you can close the test database connection.
  redisClient.quit();
});

describe('Authentication Routes', () => {
  it('should register a new user', async () => {
    const app = await loadApp(); // Load your Express app
        
    const response = await request(app)
      .post('/api/auth/register')
      // .set('Content-Type', 'application/x-www-form-urlencoded')
      .field('firstName', 'John')
      .field('lastName', 'Doe')
      .field('title', 'Software Engineer')
      .field('summary', 'A software engineer')
      .field('email', 'johndsdsdsd@example.com')
      .field('password', 'securepassword')
      .attach('image', 'uploads/image-1695896151126-694614170.jpg')

    console.log(response.body)

    expect(response.status).toBe(201); // Ensure that a valid user creation returns status 201
    expect(response.body).toHaveProperty('token');
  });

  it('should fail to register with invalid data', async () => {
    const app = await loadApp(); // Load your Express app

    const response = await request(app)
      .post('/api/auth/register')
      .send({
        // Send invalid data here (e.g., missing required fields)
      });

    expect(response.status).toBe(400); // Ensure that invalid data results in a validation error (status 400)
  });

  it('should log in an existing user', async () => {
    const app = await loadApp(); // Load your Express app

    const response = await request(app)
      // .set('Content-Type', 'application/x-www-form-urlencoded')

      .post('/api/auth/login')
      .type('form') // Set the content type to form-encoded data
      .send('email=johndsdsdsd@example.com')
      .send('password=securepassword');




    expect(response.status).toBe(200); // Ensure that a valid login returns status 200
    expect(response.body).toHaveProperty('token');
  });

  it('should fail to log in with invalid credentials', async () => {
    const app = await loadApp(); // Load your Express app

    const response = await request(app)
      .post('/api/auth/login')
      .type('form') // Set the content type to form-encoded data
      .send('email=johlkasmdlkmaslkdmalksmdlkasmdkasmdkmaskdmsdsdsdsddsd@example.com')
      .send('password=securepassword');

    expect(response.status).toBe(400); // Ensure that invalid credentials result in status 400
  });
});
