import { MigrationFn } from 'umzug';
import { DataTypes, Sequelize } from 'sequelize';

export const up: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  await q.createTable('feedbacks', {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    from_user: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false, // Remove autoIncrement and primaryKey
    },
    to_user: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false, // Remove autoIncrement and primaryKey
    },
    content: {
      type: new DataTypes.STRING(256),
      allowNull: false,
    },
    company_name: {
      type: new DataTypes.STRING(256),
      allowNull: false,
    },
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
  });
};

export const down: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  await q.dropTable('feedbacks');
};
