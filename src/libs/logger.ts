import pino from 'pino';

export const createLogger = (requestId: string) => {
  return pino({
    prettyPrint: process.env.NODE_ENV === 'development',
    base: {
      requestId, // Include the requestId in the log context
    },
  });
};

// Create a logger instance with a requestId
export const logger = createLogger('');
