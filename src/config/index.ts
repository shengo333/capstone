export interface Config {
  db: {
    host: string;
    port: number;
    username: string;
    password: string;
    database: string;
  };
  redis: {
    host: string;
    port: number;
  };
  auth: {
    secret: string;
  };
}

const configs: {
  development: Config;
} = {
  development: {
    db: {
      host: 'localhost',
      port: 3306,
      username: 'dev',
      password: 'dev',
      database: 'capstone_project',
    },
    redis: {
      host: 'localhost',
      port: 6379,
    },
    auth: {
      secret: 'some-dev-secret',
    },
  },
};

const getConfig = (): Config => {
  const env = process.env.NODE_ENV || 'development';

  if (!(env in configs)) {
    throw new Error(
      'Unsupported NODE_ENV value was provided! Possible values are "development", ...',
    );
  }

  // @ts-ignore
  return configs[env];
};

export const config = getConfig();
