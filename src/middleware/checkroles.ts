import { Request, Response, NextFunction } from 'express';

function roles(allowedRoles: string[]) {
  return (req: Request, res: Response, next: NextFunction) => {
    const user = req.user as { role: string }; // Assuming you attach the user to the request object in a previous middleware

    if (user && allowedRoles.includes(user.role)) {
      next(); // User has the required role, proceed to the endpoint
    } else {
      res.status(403).json({ error: 'Forbidden' }); // User doesn't have the required role
    }
  };
}

export default roles;
