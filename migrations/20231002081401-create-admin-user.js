const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface) => {
    const hashedPassword = await bcrypt.hash('adminpassword', 10); // Replace 'your_password_here' with the desired admin password

    return queryInterface.bulkInsert('users', [
      {
        first_name: 'Admin',
        last_name: 'User',
        email: 'admin@example.com', // Replace with the desired admin email
        password: hashedPassword,
        image: 'image-1695896151126-694614170.jpg',
        title: 'no idea what is this',
        role: 'admin',
        summary: 'this is summary',
        created_at: new Date(),
        updated_at: new Date(),
      },
    ]);
  },

  down: async (queryInterface) =>
    queryInterface.bulkDelete('Users', { email: 'admin@test.com' }),
};
